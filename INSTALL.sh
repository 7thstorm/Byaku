apt update -y
apt install imagemagick -y
npm install --unsafe-perm
npm audit fix --force
cp super.sample.json super.json
if [ ! -d "/root/darknet" ]; then
    git clone https://github.com/AlexeyAB/darknet.git /root/darknet
fi
cd /root/darknet
export PATH=/usr/local/cuda/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
sed -i 's/GPU=0/GPU=1/g' Makefile
sed -i 's/CUDNN=0/CUDNN=1/g' Makefile
if [ -x "$(command -v opencv_version)" ]; then
    sed -i 's/OPENCV=0/OPENCV=1/g' Makefile
fi
make
