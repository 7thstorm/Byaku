# Byaku : Neural Net Trainer from Shinobi

Byaku is a Neural Net Trainer UI for Object Detection engines such as Darknet or TensorFlow.

# Get Requirements The Easy Way

- Ubuntu 19.10 (Recommended Version) :
    ```
    bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/Byaku/INSTALL-ubuntu-requirements.sh)
    ```
- CentOS 7 (Recommended Version) :
    ```
    bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/Byaku/INSTALL-centos-requirements.sh)
    ```

# Requirements

- **Ubuntu 18.04 or above** (CentOS 7 should work too)
    - Download Ubuntu 19.10 ISO here : http://releases.ubuntu.com/19.10/
    - Ubuntu needs this too :
        ```
        sudo apt-get install build-essential
        ```
- **ImageMagick**
    - Install on Ubuntu ?
        ```
        apt install imagemagick -y
        ```
    - Install on CentOS ?
        ```
        yum install imagemagick -y
        ```
- **gcc/g++ +7** (for Darknet compilation)
    - Need to check version?
        - `gcc -v` and `g++ -v`
    - Need to downgrade? Run this
        ```
        bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/downgradeGccG++.sh)
        ```
- **an NVIDIA GPU, NVIDIA Drivers, CUDA Toolkit, CUDNN, CUDNN-dev**
    - Don't have the required NVIDIA software installed? Run this
        ```
        bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/cudaInstall.sh)
        ```

# How to Use

1. Download binary and place where you would like it to run. Byaku will create additional files in the location of where it is run from.
2. Make the `Byaku` app an executable.
    ```
    chmod +x ./Byaku
    ```
3. Run Byaku. It will now begin downloading base files and start building Darknet. It will only do these actions on the first start.
    ```
    ./Byaku
    ```
4. Open your web browser to the Byaku server's IP on port `8081`.
    ```
    http://xxx.xxx.xxx.xxx:8081
    ```

# Default Login

```
Username : admin
Password : admin
```
