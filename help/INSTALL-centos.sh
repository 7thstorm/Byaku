sudo yum install imagemagick -y
sudo yum groupinstall 'Development Tools' -y
sudo bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/downgradeGccG++.sh)
if [ -x "$(command -v nvidia-smi)" ]; then
    sudo bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/cudaInstall.sh)
fi
npm install --unsafe-perm
npm install pm2 -g
echo 'Done.'
echo 'Do only one of these. Start with "node app.js" or to daemonize run "pm2 start app.js"'
