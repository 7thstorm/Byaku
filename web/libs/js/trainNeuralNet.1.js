$(document).ready(function(){
    PNotify.prototype.options.styling = "fontawesome";
    $.netTrainer = {
        e: $('#trainNeuralNet'),
        matrixEditorWindow : $('#matrixEditorWindow')
    }
    $.randomString = function(x){
        if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < x; i++ )
            t += p.charAt(Math.floor(Math.random() * p.length));
        return t;
    }
    $.netTrainer.loadedImages = JSON.parse(localStorage.getItem('trainingNetImageMatrixSelections')) || {}
    $.netTrainer.matrixEditorImage = $.netTrainer.matrixEditorWindow.find('.matrix-editor img')
    // $.netTrainer.f = $.netTrainer.e.find('form')
    var imageSearchWindow = $('#imageSearchWindow')
    var searchImageField = imageSearchWindow.find('.images_search_field')
    var foundImageList = $.netTrainer.matrixEditorWindow.find('.images_search_found')
    var lastClickedBox = $.netTrainer.matrixEditorWindow.find('.images_search_last_clicked')
    var trainingButton = $.netTrainer.e.find('.trainingButton')
    $.netTrainer.trainingButton = trainingButton
    var getDimensions = function(url,callback){
        var img = new Image();
        img.onload = function(){
            callback(this.width,this.height)
        };
        img.src = url;
    }
    $.netTrainer.localStore = function(r,rr,rrr){
        if(!rrr){rrr={};};if(typeof rrr === 'string'){rrr={n:rrr}};if(!rrr.n){rrr.n='clientArea_'+location.host}
        ii={o:localStorage.getItem(rrr.n)};try{ii.o=JSON.parse(ii.o)}catch(e){ii.o={}}
        if(!ii.o){ii.o={}}
        if(r&&rr&&!rrr.x){
            ii.o[r]=rr;
        }
        switch(rrr.x){
            case 0:
                delete(ii.o[r])
            break;
            case 1:
                delete(ii.o[r][rr])
            break;
        }
        localStorage.setItem(rrr.n,JSON.stringify(ii.o))
        return ii.o
    }
    var convertDarknetImageLabelsToMatrices = function(loadedImage,callback){
        var doProcess = function(width,height){
            var matrices = []
            if(typeof loadedImage.annotation === 'string'){
                labels = loadedImage.annotation.split('\n')
            }else{
                labels = loadedImage.annotation
            }
            labels.forEach(function(label){
                if(!label)return
                var lineParts = label.split(' ')
                var classNumber = lineParts[0]
                var w = lineParts[3] * width
                var h = lineParts[4] * height
                var x = (lineParts[1] * width) - w / 2
                var y = (lineParts[2] * height) - h / 2
                matrices.push({
                    classNumber: classNumber,
                    tag: loadedImage.className,
                    x: x,
                    y: y,
                    w: w,
                    h: h,
                    imageUrl: loadedImage.url,
                    imageWidth: width,
                    imageHeight: height
                })
            })
            callback(matrices)
        }
        if(loadedImage.dimensions){
            doProcess(loadedImage.dimensions.width,loadedImage.dimensions.height)
        }else{
            getDimensions(loadedImage.url,function(width,height){
                doProcess(width,height)
            })
        }
    }

    $.netTrainer.getUploadedImages = function(callback){
        var apiUrl = '/' + $user.sessionKey+'/trainer/jpegImages/list'
        var classes = $.netTrainer.getAddedClassTags(true)
        return $.post(apiUrl,{data:JSON.stringify({classes:classes})},function(data){
            if(data.ok === false)data = []
            callback(data)
        })
    }
    var openImageInMatrixEditorCanvas = function(loadedImage){
        $.netTrainer.imageLoadedInMatrixEditor = loadedImage
        $.netTrainer.matrixEditorWindow.find('.matrix-editor').html('<div class="matrices"></div><img src="'+loadedImage.url+'">')
        var imageEl = $.netTrainer.matrixEditorWindow.find('.matrix-editor img')
        imageEl.one('load',function(){
            var imageWidthInView = imageEl.width()
            var imageHeightInView = imageEl.height()
            var imageWidth = imageEl[0].naturalWidth
            var imageHeight = imageEl[0].naturalHeight
            var xRatio = imageWidth / imageWidthInView
            var yRatio = imageHeight / imageHeightInView
            if(loadedImage && loadedImage.boundingBoxes){
                var matricesContainer = $.netTrainer.matrixEditorWindow.find('.matrices')
                $.each(loadedImage.boundingBoxes,function(n,matrix){
                    matricesContainer.append($.netTrainer.buildMatrix(matrix))
                    var newMatrixElement = matricesContainer.find('.matrix').last()
                    $.netTrainer.activateMatrix(newMatrixElement,matrix,xRatio,yRatio)
                })
            }else if(loadedImage && loadedImage.annotation){
                var matricesContainer = $.netTrainer.matrixEditorWindow.find('.matrices')
                convertDarknetImageLabelsToMatrices(loadedImage,function(matrices){
                    matrices.forEach(function(matrix){
                        matricesContainer.append($.netTrainer.buildMatrix(matrix))
                        var newMatrixElement = matricesContainer.find('.matrix').last()
                        $.netTrainer.activateMatrix(newMatrixElement,matrix,xRatio,yRatio)
                    })
                })
            }
        })
    }
    $.netTrainer.appendImageSearchPreviewItem = function(html){
        foundImageList.prepend(html)
        foundImageList.resize()
        lazyload(foundImageList
                    .find('.unLoadScrolled')
                    .removeClass('unLoadScrolled')
                    .find('img'))
    }

    $.netTrainer.matrixEditorWindow.find('.external-launch').click(function(){
        var url = $(this).parents('.modal').find('img').attr('src')
        var win = window.open(url, '_blank')
        win.focus()
    })
    $.netTrainer.e.on('click','.imageSearchPreview .delete',function(e){
        e.preventDefault()
        var el = $(this)
        var href = el.attr('href')
        $.get(href,function(data){
            console.log(data)
        })
        return false;
    })
    $.netTrainer.e.on('click','.imageSearchPreview .test',function(e){
        e.preventDefault()
        var el = $(this)
        var href = el.attr('href')
        $.confirm.create({
            title: lang.testResult,
            body: `<p>${lang.testResultText}</p><div class="text-center mb-4"><img src="${href}" style="border:0;max-width:100%;display:inline-block;border-radius: 5px;"></div><p><small>${lang.testResultText2}</small></p><p><small>${lang.testResultText3}</small></p>`,
            clickOptions: {
                class: 'btn-info',
                title: 'Done',
            },
            clickCallback: function(){}
        })
        $.confirm.e.find('.confirmaction').hide()
        return false;
    })
    $.netTrainer.e.on('click','.imageSearchPreview',function(){
        var el = $(this)
        var url = el.attr('url')
        var imageId = el.attr('imageId')
        openImageInMatrixEditorCanvas($.netTrainer.loadedImages[imageId])
        new PNotify({
            type: 'info',
            title: lang.imageLoaded,
            text: lang.imageLoadedText
        })
    })
    foundImageList.resize(function(){
        var height = $('.imageSearchPreview:first').width()
        foundImageList.find('.imageSearchPreview').css('height',height)
    })
    //
    $.netTrainer.selectedMatrixCanvas = {}
    $.netTrainer.activateMatrix = function(newMatrixElement,matrix,xRatio,yRatio){
        newMatrixElement.draggable({
            handle: ".handle",
            containment: "parent",
            start: function() {
                $.netTrainer.saveImageViewerMatrices()
            },
            stop: function() {
                $.netTrainer.saveImageViewerMatrices()
            }
        })
        if(matrix){
            if(!matrix.w)matrix.w = 100
            if(!matrix.h)matrix.h = 100
            if(!matrix.x)matrix.x = 100
            if(!matrix.y)matrix.y = 100
            if(!matrix.tag)matrix.tag = ''
            newMatrixElement.css({
                width: matrix.w / xRatio + 'px',
                height: matrix.h / yRatio + 'px',
                top: matrix.y / yRatio + 'px',
                left: matrix.x / xRatio + 'px',
            })
        }
        console.log('resizable')
        newMatrixElement.resizable({
            containment: "#matrixEditorWindow .matrix-editor .matrices",
            handles: 'n, e, s, w, ne, nw, se, sw',
            stop: function() {
                $.netTrainer.saveImageViewerMatrices()
            }
        })

    }
    $.netTrainer.buildMatrix = function(matrix){
        var currentImage = $.netTrainer.matrixEditorWindow.find('.matrix-editor img')
        if(!matrix)matrix = {}
        if(!matrix.w)matrix.w = '100'
        if(!matrix.h)matrix.h = '100'
        // if(!matrix.x)matrix.x = '100'
        // if(!matrix.y)matrix.y = '100'
        if(!matrix.tag)matrix.tag = ''
        var html = `<div class='matrix selected' style="left: ${matrix.x}px; top: ${matrix.y}px; width: ${matrix.w}px; height: ${matrix.h}px;">
                      <div class="handle"><a class="delete pull-right float-right">&nbsp;<i class="fa fa-times"></i>&nbsp;</a></div>
                      <input class="form-control" value="${matrix.tag}" placeholder="Object Tag">
                      <div class='resizers'>
                        <div class='resizer bottom-right'></div>
                      </div>
                    </div>`
        return html
    }
    $.netTrainer.matrixEditorWindow.on('click','.addMatrix',function(){
        var el = $(this)
        var matricesContainer = $.netTrainer.matrixEditorWindow.find('.matrices')
        matricesContainer.find('.matrix').removeClass('selected')
        matricesContainer.append($.netTrainer.buildMatrix({
            tag: searchImageField.val()
        }))
        var newMatrixElement = matricesContainer.find('.matrix').last()
        $.netTrainer.activateMatrix(newMatrixElement)
    })
    $.netTrainer.matrixEditorWindow.on('change','.matrix input',function(){
        $.netTrainer.saveImageViewerMatrices()
    })
    $.netTrainer.matrixEditorWindow.on('click','.delete',function(){
        var el = $(this).parents('.matrix')
        el.remove()
        $.netTrainer.saveImageViewerMatrices()
    })
    $.netTrainer.matrixEditorWindow.on('click','.matrix',function(){
        $.netTrainer.matrixEditorWindow.find('.matrix.selected').removeClass('selected')
        $(this).addClass('selected')
    })
    $.netTrainer.matrixEditorDrawing = false
    // $.netTrainer.matrixEditorWindow.on('contextmenu','.matrices',function(e){
    //     e.preventDefault()
    //     var _this = this
    //     $.netTrainer.matrixEditorDrawingCurrentStartX = e.pageX - _this.offsetLeft
    //     $.netTrainer.matrixEditorDrawingCurrentStartY = e.pageY - _this.offsetTop
    //     $(this).append($.netTrainer.buildMatrix({
    //         x : 0,
    //         y : 0,
    //         w : 0,
    //         h : 0
    //     }))
    //     $.netTrainer.activateMatrix($(_this).find('.matrix').last())
    // })
    $.netTrainer.saveImageViewerMatrices = function(){
        var overallClassesLoaded = $.netTrainer.getAddedClassTags(false)
        var loadedImage = $.netTrainer.imageLoadedInMatrixEditor
        var matricesFoundInView = {
            boundingBoxes: []
        }
        var matricesContainer = $.netTrainer.matrixEditorWindow.find('.matrices')
        var matricesInView = matricesContainer.find('.matrix')
        var imageEl = $.netTrainer.matrixEditorWindow.find('.matrix-editor img')
        var imageWidthInView = imageEl.width()
        var imageHeightInView = imageEl.height()
        var imageWidth = imageEl[0].naturalWidth
        var imageHeight = imageEl[0].naturalHeight
        var xRatio = imageWidth / imageWidthInView
        var yRatio = imageHeight / imageHeightInView
        var dataAnnotation = []
        matricesInView.each(function(n,el){
            var matrix = $(el)
            var position = matrix.position()
            var width = matrix.width()
            var height = matrix.height()
            var tag = matrix.find('input').val().toLowerCase()
            if(tag !== ''){
                var classAnnotationPosition = overallClassesLoaded.indexOf(tag)
                if(classAnnotationPosition === -1){
                    overallClassesLoaded.push(tag)
                    classAnnotationPosition = overallClassesLoaded.indexOf(tag)
                    $.netTrainer.classesField.tagEditor('addTag',tag, true)
                    $.netTrainer.saveClassesToLocalStorage()
                    $.netTrainer.createDarknetCfg()
                }
                // var matrixAsAnnotation = `${classAnnotationPosition} ${(position.left + (width / 2)) / imageWidthInView} ${(position.top + (height / 2)) / imageHeightInView} ${width / imageWidthInView} ${height / imageHeightInView}`
                var matrixAsAnnotation = [
                    classAnnotationPosition,
                    ((position.left + (width / 2)) / imageWidthInView) * xRatio,
                    ((position.top + (height / 2)) / imageHeightInView) * yRatio,
                    (width / imageWidthInView) * xRatio,
                    (height / imageHeightInView) * yRatio
                ].join(' ')
                dataAnnotation.push(matrixAsAnnotation)
                matricesFoundInView.boundingBoxes.push({
                    h: height * yRatio,
                    w: width * xRatio,
                    x: position.left * xRatio,
                    y: position.top * yRatio,
                    tag: tag,
                    annote: matrixAsAnnotation
                })
            }else{
                console.log('Object Tag Empty!')
            }
        })
        loadedImage.dataAnnotation = dataAnnotation.join('\n')
        loadedImage.boundingBoxes = matricesFoundInView.boundingBoxes
        loadedImage.imageWidthInView = imageWidthInView
        loadedImage.imageHeightInView = imageHeightInView
        $.netTrainer.saveLoadedImageDataToLocalStorage()
        return loadedImage
    }
    $.netTrainer.saveLoadedImageDataToLocalStorage = function(){
        localStorage.setItem('trainingNetImageMatrixSelections',JSON.stringify($.netTrainer.getLoadedImagesWithBoundingBoxes()))
    }
    $.netTrainer.getLoadedImagesWithBoundingBoxes = function(asArray){
        var loadedImagesWithBoxes = {}
        if(asArray)loadedImagesWithBoxes = []
        $.each($.netTrainer.loadedImages,function(imageId,loadedImage){
            if(loadedImage.boundingBoxes){
                if(asArray){
                    loadedImagesWithBoxes.push(loadedImage)
                }else{
                    loadedImagesWithBoxes[imageId] = loadedImage
                }
            }
        })
        return loadedImagesWithBoxes
    }
    $.netTrainer.loadLocalImages = function(){
        $.netTrainer.getUploadedImages(function(imageList){
            foundImageList.find('[source="Uploaded"]').remove()
            $.each(imageList,function(n,image){
                $.netTrainer.appendImageSearchPreviewItem($.imageManager.buildImageSearchPreviewItem(image))
                if(!$.netTrainer.loadedImages[image.source + image.name]){
                    $.netTrainer.loadedImages[image.source + image.name] = {
                        url: image.url,
                        source: image.source,
                        preview: image.preview
                    }
                }else{
                    $.netTrainer.loadedImages[image.source + image.name].url = image.url
                }
            })
        })
    }
    // $.netTrainer.getDarknetCfg(function(response){
    //     $.netTrainer.createDarknetCfgOutputElement.html(response.cfg.replace(/\n/g,'<br>'))
    // })
    // Fullscreen
    function openFullscreen(elem) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }
    function closeFullscreen() {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
      }
    }
    $('body')
        .on('click','.fullscreen-element',function(e){
            e.preventDefault()
            var element = $(this).parents('.fullscreen-target')
            element.find('.unfullscreen').show()
            openFullscreen(element[0])
            fullscreenCloser.show()
            return false
        })
        .on('click','.unfullscreen',function(e){
            e.preventDefault()
            closeFullscreen()
            $('.unfullscreen').hide()
            return false
        })
        .on('click','[system]',function(e){
            switch($(this).attr('system')){
                case'openLogs':
                    $.get(`/${$user.sessionKey}/systemLogs`,function(data){
                        $.confirm.create({
                            title: 'Logs',
                            body: `<pre class="output_data" style="max-height:500px">${JSON.stringify(data.reverse(),null,3)}</pre>`,
                            clickOptions: {
                                class: 'btn-info',
                                title: 'Done',
                            },
                            clickCallback: function(){}
                        })
                        $.confirm.e.find('.confirmaction').hide()
                    })
                break;
            }
        })
})
