$(document).ready(function(){
    $.createMenuItemContents = function(contents){
        var html = `<div class="icon icon-shape bg-gradient-${contents.type} rounded-circle text-white">
                <i class="fa fa-${contents.icon}"></i>
              </div>
              <div class="media-body ml-3">
                <h5 class="heading text-${contents.type} mb-md-1">${contents.title}</h5>
                <p class="description d-none d-md-inline-block mb-0">${contents.text}</p>
              </div>`
        if(contents.addShell === true){
            html = `<a class="${contents.class} media d-flex align-items-center">` + html + `</a>`
        }
        return html
    }
})
