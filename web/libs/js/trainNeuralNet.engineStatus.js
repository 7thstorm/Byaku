$(document).ready(function(){
    var statusWindow = $('#statusWindow')
    var currentDataSet = [0,0,0,0,0,0,0,0,0]
    $.statusWindow = {
        hidden: true
    }
    var options = {
      series: [{
      data: currentDataSet
    }],
      chart: {
      id: 'realtime',
      height: 350,
      type: 'line',
      animations: {
        enabled: false
      },
      toolbar: {
        show: false
      },
      zoom: {
        enabled: false
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth'
    },
    title: {
      text: 'Total Loss on Testing',
      align: 'center'
    },
    markers: {
      size: 0
    },
    legend: {
      show: false
    },
    };

    var chart = new ApexCharts(document.querySelector("#loss-chart"), options);
    chart.render();

    $.statusWindow.lossChartAddNewValue = function(newValue){
        currentDataSet.shift()
        currentDataSet.push(newValue)
        chart.updateSeries([{
          data: currentDataSet
        }])
    }
    statusWindow.on('shown.bs.modal', function (e) {
        $.statusWindow.hidden = false
    })
    statusWindow.on('hidden.bs.modal', function (e) {
        $.statusWindow.hidden = true
    })
})
