$(document).ready(function(){
    var imageSearchWindow = $('#imageSearchWindow')
    var searchImageField = imageSearchWindow.find('.images_search_field')
    var searchImageList = imageSearchWindow.find('.images_search_found')
    var searchImageLimit = imageSearchWindow.find('.images_search_limit')
    var searchInternetImages = function(term,callback){
        return $.get('/' + $user.sessionKey+'/trainer/searchInternetImages?search=' + term,function(data){
            callback(data)
        })
    }
    var buildImageSearchPreviewItems = function(pics){
        var html = ''
        $.each(pics,function(key,pic){
            var filename = pic.url.split('/')
            filename = filename[filename.length - 1]
            if(!$.netTrainer.loadedImages[pic.source + filename]){
                $.netTrainer.loadedImages[pic.source + filename] = {
                    url: pic.url,
                    sourceData: pic,
                    hasAnnotation: pic.hasAnnotation
                }
            }
            html += buildImageSearchPreviewItem(pic)
        })
        return html
    }
    var buildImageSearchPreviewItem = function(pic){
        var filename = pic.url.split('/')
        filename = filename[filename.length - 1]
        var imageId = pic.source + filename
        var additonalClasses = []
        if(pic.hasAnnotation)additonalClasses.push('hasAnnotation')
        var existingElement = searchImageList.find(`[imageId="${imageId}"]`)
        if(existingElement.length > 0){
            existingElement.remove()
        }
        var html = `<div class="imageSearchPreview unLoadScrolled ${additonalClasses.join(' ')}" imageId="${imageId}" url="${pic.url}" source="${pic.source}">
            <div class="corner-control">
                <a class="btn btn-sm btn-info test text-white" target="_blank" title="${lang['Object Detection Test']}" href="${$user.sessionKey}/trainer/test?imageUrl=${pic.url}"><i class="fa fa-search"></i></a>
            </div>
            <small>${pic.source}</small>
            <img data-src="${pic.preview}" class="lazyload">
        </div>`
        return html
    }
    var appendImageSearchPreviewItem = function(html){
        searchImageList.prepend(html)
        searchImageList.resize()
        lazyload(searchImageList
                    .find('.unLoadScrolled')
                    .removeClass('unLoadScrolled')
                    .find('img'))
    }
    searchImageField.change(function(){
        var el = $(this)
        var thisVal = el.val()
        searchInternetImages(thisVal,function(data){
            searchImageList.find('[source="pixabay"]').remove()
            appendImageSearchPreviewItem(buildImageSearchPreviewItems(data))
        })
    })
    searchImageList.resize(function(){
        var height = $('.imageSearchPreview:first').width()
        searchImageList.find('.imageSearchPreview').css('height',height)
    })
})
