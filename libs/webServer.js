var fs = require('fs')
var http = require('http')
var https = require('https')
var express = require('express')
var app = express()
var bodyParser = require('body-parser');
var ejs = require('ejs');
module.exports = function(s,config,lang,io){
    var server = http.createServer(app)
    server.listen(config.port,function(){
        s.appLog(config.webPageTitle + ' : Web Server Listening on ' + config.port)
    })
    io.attach(server,{
        transports: ['websocket']
    })
    io.engine.ws = new (require('cws').Server)({
        noServer: true,
        perMessageDeflate: false
    })
    s.renderPageGlobalPassables = {}
    s.renderPage = function(req,res,paths,passables,callback){
        if(!passables)passables = {}
        passables.config = config
        if(!passables.lang)passables.lang = lang
        if(!passables.window)passables.window = {}
        passables = Object.assign(passables,s.renderPageGlobalPassables)
        res.render(paths,passables,callback)
    }
    s.getPostData = function(req,target,parseJSON){
        if(!target)target = 'data'
        if(!parseJSON)parseJSON = true
        var postData = false
        if(req.query && req.query[target]){
            postData = req.query[target]
        }else{
            postData = req.body[target]
        }
        if(parseJSON === true){
            postData = s.parseJSON(postData)
        }
        return postData
    }
    s.closeJsonResponse = function(res,endData){
        res.setHeader('Content-Type', 'application/json')
        res.end(s.prettyPrint(endData))
    }
    s.getClientIp = function(req){
        return req.headers['cf-connecting-ip'] ||
            req.headers["CF-Connecting-IP"] ||
            req.headers["'x-forwarded-for"] ||
            req.connection.remoteAddress
    }
    app.enable('trust proxy')

    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({extended: true}))
    // app.use(function (req,res,next){
    //     res.header("Access-Control-Allow-Origin",req.headers.origin)
    //     next()
    // })
    app.set('views', __dirname + '/../web')
    app.set('view engine','ejs')
    app.use('/libs',express.static(__dirname + '/../web/libs'))
    require('./webServer/paths.js')(s,app,lang,config,io)
    return app
}
