module.exports = function(s,config,lang,io){
    //send data to socket client function
    s.tx = function(z,y,x){if(x){return x.broadcast.to(y).emit('f',z)};io.to(y).emit('f',z);}
    s.sendDataToAllConnectedClients = (pointer,data)=>{
        io.emit(pointer,data)
    }
    s.sendDashboardNotification = (data)=>{
        io.emit('notification',data)
    }
    ////socket controller
    io.on('connection', function (cn) {
        // super page socket functions
        cn.on('super',function(d){
            if(!cn.init && d.f == 'init'){
                d.ok=s.superAuth({mail:d.mail,pass:d.pass},function(data){
                    cn.mail = d.mail
                    cn.join('$')
                    var tempSessionKey = s.gid(30)
                    cn.superSessionKey = tempSessionKey
                    s.superUsersApi[tempSessionKey] = data
                    s.superUsersApi[tempSessionKey].cnid = cn.id
                    if(!data.$user.tokens)data.$user.tokens = {}
                    data.$user.tokens[tempSessionKey] = {}
                    cn.ip = cn.request.connection.remoteAddress
                    cn.init = 'super'
                    s.tx({
                        f: 'init_success',
                        mail: d.mail,
                        superSessionKey: tempSessionKey,
                        engineStatus: s.engineStatus
                    },cn.id)
                })
                if(d.ok === false){
                    cn.disconnect()
                }
            }
        })
        cn.on('disconnect', function () {
            if(cn.superSessionKey){
                delete(s.superUsersApi[cn.superSessionKey])
            }
        })
    })
}
