var fs = require('fs')
var execSync = require('child_process').execSync
var AdmZip = require('adm-zip');
module.exports = function(config){
    const activeDirectory = config.activeFilesDir
    const compiledDirectory = `./compiled`
    const modelDirectory = `${compiledDirectory}/models/`
    return {
        create: (callback)=>{
            const classesFilePath = `${activeDirectory}classes.names`
            const builtCfgPath = `${activeDirectory}yolov3-built.cfg`
            const lastBuiltWeightsPath = `${activeDirectory}weights/yolov3-built_last.weights`
            if(!fs.existsSync(compiledDirectory)){
                fs.mkdirSync(compiledDirectory)
            }
            fs.unlink(modelDirectory,()=>{
                execSync(`rm -rf "${modelDirectory}"`)
                fs.mkdirSync(modelDirectory)
                if(!fs.existsSync(modelDirectory + 'data')){
                    fs.mkdirSync(modelDirectory + 'data')
                }
                if(!fs.existsSync(modelDirectory + 'cfg')){
                    fs.mkdirSync(modelDirectory + 'cfg')
                }
                if(!fs.existsSync(classesFilePath) || !fs.existsSync(builtCfgPath) || !fs.existsSync(lastBuiltWeightsPath)){
                    return callback({
                        ok: false,
                        msg: 'Cannot find required files. Did you train anything yet?'
                    })
                }
                var classes = fs.readFileSync(classesFilePath,'utf8')
                var classesArray = classes.trim().split('\n')
                var outputCfgLocation = `${modelDirectory}cfg/yolov3.cfg`
                var outputCocoNamesPath = modelDirectory + `data/coco.names`
                fs.writeFileSync(outputCocoNamesPath,classes,'utf8')

                var sourceCfgData = fs.readFileSync(builtCfgPath,'utf8')
                var processedCfgData = sourceCfgData
                                          // disable training subdivision setting and use 1 for test
                                          // zubdivisionz has no meaning. It is just a placeholder
                                          .replace('# subdivisions=1','# zubdivisionz=1')
                                          .replace('subdivisions=','# subdivisions=')
                                          .replace('# zubdivisionz=1','subdivisions=1')
                                          // disable training batch setting and use 1 for test
                                          // catcz has no meaning. It is just a placeholder
                                          .replace('# batch=1','# catcz=1')
                                          .replace('batch=','# batch=')
                                          .replace('# catcz=1','batch=1')
                fs.writeFileSync(outputCfgLocation,processedCfgData)
                fs.createReadStream(lastBuiltWeightsPath).pipe(fs.createWriteStream(modelDirectory + `yolov3.weights`))

                var cocoData = ''
                cocoData += `classes = ${classesArray.length}\n`
                cocoData += `train = placeHolderData.txt\n`
                cocoData += `valid = placeHolderData.txt\n`
                cocoData += `names = data/coco.names\n`
                cocoData += `backup = weights/`

                fs.writeFileSync(modelDirectory + `cfg/coco.data`,cocoData,'utf8')
                callback({
                    ok: true
                })
            })
        },
        clean: (callback)=>{
            fs.unlink(modelDirectory,()=>{
                callback({
                    ok: err ? false : true,
                    err: err ? err : null
                })
            })
        },
        // createZip: ()=>{
        //     const modelsZipPath = `${compiledDirectory}/models.zip`
        //     var zip = new AdmZip();
        //     var content = "inner content of the file";
        //     zip.addFile("test.txt", Buffer.alloc(content.length, content), "entry comment goes here");
        // }
    }
}
