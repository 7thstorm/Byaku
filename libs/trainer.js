var request = require('request')
var fs = require('fs')
var moment = require('moment')
var express = require('express')
var spawn = require('child_process').spawn
var exec = require('child_process').exec
var execSync = require('child_process').execSync
var fileUpload = require('express-fileupload')
var sizeOf = require('image-size');
let xmlParser = require('xml2json');
let csvParser = require('csv-parser');
const readline = require('readline');
module.exports = function(s,config,lang,app,io){
    if(!config.darknetDir){config.darknetDir = './darknet/darknet-master/'}else{config.darknetDir = s.checkCorrectPathEnding(config.darknetDir)}
    if(!config.toolsDir){config.toolsDir = __dirname + '/../tools/'}else{config.toolsDir = s.checkCorrectPathEnding(config.toolsDir)}
    if(!config.activeFilesDir){config.activeFilesDir = './activeFiles/'}else{config.activeFilesDir= s.checkCorrectPathEnding(config.activeFilesDir)}
    if(!config.darknetBase){
        config.darknetBase = [
            `darknet53.conv.74`,
            'resnet50.65',
            'yolov3-tiny.conv.15',
            'yolov3-spp.conv.85',
        ]
    }
    if(!config.baseWeightsDownloadUrl){config.baseWeightsDownloadUrl = `http://cdn.shinobi.video/weights/${config.darknetBase}`}
    var openImagesDownloadProcess = null
    var openImagesDownloadAllowed = null
    var darknetCommand = config.darknetDir + 'darknet'
    var jpegImagesFolder = config.activeFilesDir + 'JPEGImages/'
    var labelsFolder = config.activeFilesDir + 'labels/'
    var classesNamesFile = config.activeFilesDir + 'classes.names'
    var darknetDataFilename = config.activeFilesDir + 'darknet.data'
    var yoloCfgPath = config.activeFilesDir + 'yolov3-built.cfg'
    var weightsPath = config.activeFilesDir + 'weights'
    var backupWeightsPath = config.activeFilesDir + 'backupWeights/'
    var baseWeightsPath = config.activeFilesDir
    var trainTxtFilename = `${config.activeFilesDir}train.txt`
    var testTxtFilename = `${config.activeFilesDir}test.txt`
    var annotationBuildClassesHistoryPath = config.activeFilesDir + '/annotionBuildHistoryClasses.json'
    var manuallyUploadedFilesPath = config.activeFilesDir + '/manuallyUploadedFiles.json'
    const modelManager = require('./modelManager.js')(config);
    s.appLog('Darknet Bases : ',config.darknetBase)
    s.engineStatus = {
        trainingDarknet: false,
        downloadingOpenImages: false,
        currentTrainingSession: {}
    }
    //
    var getDarknetCfgBase = function(base){
        var cfgPath
        switch(base){
            case'resnet50.65':
                cfgPath = config.toolsDir + 'resnet50.cfg'
            break;
            case'yolov3-tiny.conv.15':
                cfgPath = config.toolsDir + 'yolov3-gen-tiny.cfg'
            break;
            case'yolov3-spp.conv.85':
                cfgPath = config.toolsDir + 'yolov3-spp.cfg'
            break;
            default: //darknet53.conv.74
                cfgPath = config.toolsDir + 'yolov3-gen.cfg'
            break;
        }
        return fs.readFileSync(cfgPath,'utf8')
    }
    var loadCsv = function(options,callback){
        var results = []
        if(!options){
            options = {}
        }
        fs.createReadStream(options.path)
          .pipe(csvParser(options.headers))
          .on('data', (data) => results.push(data))
          .on('end', () => {
              if(callback)callback(results)
          })
    }
    var downloadFile = function(url,outputLocation,callback){
        var r = request.get(url,function(){
            if(callback)callback()
        })
        r.on('response',  function (res) {
            res.pipe(fs.createWriteStream(outputLocation))
        })
    }
    var downloadFileStream = function(url,outputLocation,callback){
        var r = request(url)
        var theTimeout
        r.on('data',  function (res) {
            clearTimeout(theTimeout)
            theTimeout = setTimeout(()=>{
                if(callback)callback()
            },2000)
        })
        r.on('response',  function (res) {
            res.pipe(fs.createWriteStream(outputLocation))
        })
    }
    var getLabelFileName = function(string){
        return string.replace(/.jpeg/g,'.txt').replace(/.jpg/g,'.txt')
    }
    if(config.darknetBase instanceof Array){
        config.darknetBase.forEach(function(base){
            var downloading = 0
            if(!fs.existsSync(baseWeightsPath + base)){
                ++downloading
                s.appLog('Downloading : ',base)
                s.sendDashboardNotification({
                    type: 'info',
                    title: 'Downloading : ',base,
                    text: lang['Please Wait for Base Weights to Download']
                })
                downloadFile(`http://cdn.shinobi.video/weights/${base}`,baseWeightsPath + base,function(){
                    s.appLog('Downloaded : ',base)
                    s.sendDashboardNotification({
                        type: 'success',
                        title: 'Downloaded : ' + base,
                        text: lang['Base weight is ready for use.'] + ' (' + base + ')'
                    })
                })
            }
            if(downloading > 0)s.appLog(lang['Please Wait for Base Weights to Download'])
        })
    }
    if(!fs.existsSync(config.activeFilesDir)){
        fs.mkdirSync(config.activeFilesDir)
    }
    if(!fs.existsSync(jpegImagesFolder)){
        fs.mkdirSync(jpegImagesFolder)
    }
    if(!fs.existsSync(labelsFolder)){
        fs.mkdirSync(labelsFolder)
    }
    if(!fs.existsSync(weightsPath)){
        fs.mkdirSync(weightsPath)
    }
    if(!fs.existsSync(backupWeightsPath)){
        fs.mkdirSync(backupWeightsPath)
    }
    var getAnnotationClassesHistory = function(){
        var annotationBuildClassesHistory
        try{
            annotationBuildClassesHistory = JSON.parse(fs.readFileSync(annotationBuildClassesHistoryPath))
        }catch(err){
            annotationBuildClassesHistory = {}
        }
        return annotationBuildClassesHistory
    }
    var getManuallyUploadedFileList = function(){
        var manuallyUploadedFiles
        try{
            manuallyUploadedFiles = JSON.parse(fs.readFileSync(manuallyUploadedFilesPath))
        }catch(err){
            manuallyUploadedFiles = {}
        }
        return manuallyUploadedFiles
    }
    var writeManuallyUploadedFileList = function(fullListData){
        fs.writeFile(manuallyUploadedFilesPath,s.prettyPrint(fullListData),function(err){
            if(err)s.appLog(err)
        })
    }
    //
    var sendDataToConnectedSuperUsers = function(data){
        return s.tx(data,'$')
    }
    var sendDataToConnectedSuperUsersWithFilters = function(data,filtered){
        //{{SESSION_KEY}}
        Object.keys(s.superUsersApi).forEach(function(key){
            var dataCopy = Object.assign(data,{})
            var session = s.superUsersApi[key]
            filtered.forEach(function(filterKey){
                dataCopy[filterKey] = dataCopy[filterKey].replace('{{SESSION_KEY}}',key)
            })
            s.tx(dataCopy,session.cnid)
        })
    }
    var isMoreThan24HoursAgo = function(date) {
        var day = 1000 * 60 * 60 * 24
        var dayago = Date.now() - day
        return date > dayago
    }
    // var searchBingImages = function(term,callback){
    //     if(!config.bingSubscriptionKey) return s.appLog('No Bing Subscription Key')
    //     var subscriptionKey = config.bingSubscriptionKey;
    //     var url = 'http://api.cognitive.microsoft.com/bing/v7.0/images/search'
    //     request({
    //         url : url,
    //         headers : {
    //             'Ocp-Apim-Subscription-Key' : subscriptionKey,
    //         }
    //     },function(err,resp,data){
    //         var json = s.parseJSON(data)
    //         s.appLog(json)
    //         callback(err,json)
    //     })
    // }
    // searchBingImages('ocean')
    var pixaBayCache = {}
    var searchPixaBay = function(search,callback){
        if(!config.pixaBayApiKey){
            callback(null,[])
            return s.appLog('No PixaBay Subscription Key')
        }
        var url = 'https://pixabay.com/api/?key=' + config.pixaBayApiKey + '&q=' + encodeURIComponent(search.term) + '&pretty=true&per_page='+search.limit
        if(
            !pixaBayCache[search.term] ||
            isMoreThan24HoursAgo(pixaBayCache[search.term].time) &&
            search.limit !== pixaBayCache[search.term].limit
        ){
            request(url,function(err,resp,data){
                var json = s.parseJSON(data)
                var processedHits = []
                json.hits.forEach(function(hit){
                    processedHits.push({
                        preview: hit.previewURL,
                        url: hit.largeImageURL,
                        tags: hit.tags,
                        height: hit.imageHeight,
                        width: hit.imageWidth,
                        type: hit.type,
                        source: 'pixabay'
                    })
                })
                pixaBayCache[search.term] = {
                    time: new Date(),
                    hits: json,
                    limit: search.limit,
                    processedHits: processedHits
                }
                callback(err,processedHits)
            })
        }else{
            callback(null,pixaBayCache[search.term].processedHits)
        }
    }
    var getLocalImagesBasedOnClass = function(options,callback){
        var response = {ok: false}
        if(!options)options = {}
        if(!options.classes)options.classes = []
        var imagesList = []
        if(weightTrainingProcess === null){
            //get open image files
            var classDescriptionBoxableCsvFilename = `class-descriptions-boxable.csv`
            var classDescriptionBoxableCsvFilePath = `${config.activeFilesDir}${classDescriptionBoxableCsvFilename}`
            var trainAnnotationsBboxCsvFilename = `train-annotations-bbox.csv`
            var trainAnnotationsBboxCsvFilePath = `${config.activeFilesDir}${trainAnnotationsBboxCsvFilename}`
            var classDescriptionBoxableJson = {}
            var classDescriptionBoxableCsv = []
            try{
                classDescriptionBoxableCsv = fs.readFileSync(classDescriptionBoxableCsvFilePath,'utf8').toString().split('\n')
            }catch(err){

            }
            var trainAnnotationsBboxCsvFileData = []
            var annotationReaderTimeout
            const rl = readline.createInterface({
                input: fs.createReadStream(trainAnnotationsBboxCsvFilePath),
                output: process.stdout,
                terminal: false
            });

            rl.on('line', (line) => {
                trainAnnotationsBboxCsvFileData.push(line)
                clearTimeout(annotationReaderTimeout)
                annotationReaderTimeout = setTimeout(()=>{
                    moveOnward()
                },2000)
            });
            const moveOnward = () => {
                classDescriptionBoxableCsv.forEach(function(row){
                    var split = row.split(',')
                    if(split[1] && split[0])classDescriptionBoxableJson[split[1]] = split[0]
                })
                options.classes.forEach(function(className,ind){
                    if(classDescriptionBoxableJson[className]){
                        var classAnnotations = trainAnnotationsBboxCsvFileData.filter(line => line.indexOf(classDescriptionBoxableJson[className]) > -1)
                        classAnnotations.forEach(function(line,lineInd){
                            var lineParts = line.split(',')
                            if(!lineParts[2]){
                                return
                            }
                            var filename = `${lineParts[0]}.jpg`
                            var filePath = `${jpegImagesFolder}${filename}`
                            var labelFilePath = `${labelsFolder}${lineParts[0]}.txt`
                            if(fs.existsSync(filePath)){
                                imagesList.push(filename)
                            }
                            // if(ind === options.classes.length - 1 && lineInd === classAnnotations.length - 1 && callback){
                            //     callback(imagesList)
                            // }
                        })
                    }
                })
                //get custom added files
                var annotationBuildClassesHistory = getAnnotationClassesHistory()
                Object.keys(annotationBuildClassesHistory).forEach(function(tag){
                    Object.keys(annotationBuildClassesHistory[tag]).forEach(function(filename){
                        var filePath = jpegImagesFolder + filename
                        if(imagesList.indexOf(filename) === -1 && fs.existsSync(filePath)){
                            imagesList.push(filename)
                        }
                    })
                })
                if(callback)callback(imagesList)
            }
        }else{
            if(callback)callback(imagesList)
        }
        return imagesList
    }
    // var rebuildClassPositions = function(options,callback){
    //     //!!!!! NOT DONE
    //     var response = {ok: false}
    //     if(!options)options = {}
    //     if(!options.classes)options.classes = []
    //     var imageList = getLocalImagesBasedOnClass(options)
    //     imageList.forEach(function(filename){
    //         var labelName = getLabelFileName(filename)
    //         var labelPath = labelsFolder + labelName
    //         var imageId = labelName.replace('.txt','')
    //         var classDescriptionBoxableCsvFilename = `class-descriptions-boxable.csv`
    //         var classDescriptionBoxableCsvFilePath = `${config.activeFilesDir}${classDescriptionBoxableCsvFilename}`
    //         var trainAnnotationsBboxCsvFilename = `train-annotations-bbox.csv`
    //         var trainAnnotationsBboxCsvFilePath = `${config.activeFilesDir}${trainAnnotationsBboxCsvFilename}`
    //         var commandStr = `grep "${imageId}" ${trainAnnotationsBboxCsvFilePath}`
    //         var imageAnnotations = execSync(commandStr,'utf8').toString()
    //         if(imageAnnotations.length > 5){
    //             //is open images
    //             var imageAnnotationLines = imageAnnotations.split('\n')
    //             imageAnnotationLines.forEach(function(line){
    //                 var lineParts = line.split(',')
    //                 var labelFile = `${labelsFolder}${imageId}.txt`
    //                 fs.unlink(labelFile,function(){
    //                     var annotationContents = [ind,'' + (parseFloat(lineParts[5]) + parseFloat(lineParts[4])/2), '' + (parseFloat(lineParts[7]) + parseFloat(lineParts[6])/2), '' + (parseFloat(lineParts[5])-parseFloat(lineParts[4])),'' + (parseFloat(lineParts[7])-parseFloat(lineParts[6]))].join(' ')
    //                     fs.writeFileSync(labelFile,annotationContents)
    //                 })
    //             })
    //         }else{
    //             //is not open images
    //         }
    //     })
    // }
    var runOpenImagesDownload = function(options,callback){
        var response = {ok: false}
        if(!config.userHasSubscribed){
            s.sendDashboardNotification({
                type: 'warning',
                title: lang.featureUnavailable,
                text: lang.onlyAvailableToSubscribers + ' ' + lang.onlyAvailableToSubscribers2
            })
            response.msg = lang.onlyAvailableToSubscribers + ' ' + lang.onlyAvailableToSubscribers2
            callback(response)
            return
        }
        if(!options)options = {}
        if(!options.runMode)options.runMode = 'train'
        if(!options.imageBase)options.imageBase = 'v5'
        if(!options.classes)options.classes = []
        var classes = options.classes
        if(classes instanceof Array){
            classes = options.classes.join(',')
        }
        openImagesDownloadAllowed = true
        if(!openImagesDownloadProcess){
            openImagesDownloadProcess = true
            s.engineStatus.downloadingOpenImages = true
            var openImagesEndpoint = `https://storage.googleapis.com/openimages/`
            var classDescriptionBoxableCsvFilename = `class-descriptions-boxable.csv`
            var classDescriptionBoxableCsvFilePath = `${config.activeFilesDir}${classDescriptionBoxableCsvFilename}`
            var trainAnnotationsBboxCsvFilename = `train-annotations-bbox.csv`
            var trainAnnotationsBboxCsvFilePath = `${config.activeFilesDir}${trainAnnotationsBboxCsvFilename}`
            var trainAnnotationsBboxHeaders = ["ImageID", "Source", "LabelName", "Confidence", "XMin", "XMax", "YMin", "YMax", "IsOccluded", "IsTruncated", "IsGroupOf", "IsDepiction", "IsInside"]
            var sendData = function(subAction,data){
                s.appLog('openImagesDownload',subAction,data)
                sendDataToConnectedSuperUsers({
                    f: 'openImagesDownload',
                    ff: subAction,
                    data: data
                })
            }
            switch(options.imageBase){
                case'2018_04':
                case'v4':
                    sendData('stdout',`Downloading OpenImages v4`)
                    classDescriptionBoxableCsvUrl = `${openImagesEndpoint}2018_04/class-descriptions-boxable.csv`
                    trainAnnotationsBboxCsvUrl = `${openImagesEndpoint}2018_04/train/train-annotations-bbox.csv`
                break;
                case'v5':
                    sendData('stdout',`Downloading OpenImages v5`)
                    classDescriptionBoxableCsvUrl = `${openImagesEndpoint}v5/class-descriptions-boxable.csv`
                    trainAnnotationsBboxCsvUrl = `${openImagesEndpoint}2018_04/train/train-annotations-bbox.csv`
                break;
            }
            if(!fs.existsSync(classDescriptionBoxableCsvFilePath)){
                sendData('stdout',`Downloading : ${classDescriptionBoxableCsvUrl}`)
                downloadFile(classDescriptionBoxableCsvUrl, classDescriptionBoxableCsvFilePath)
            }
            if(!fs.existsSync(trainAnnotationsBboxCsvFilePath)){
                s.sendDashboardNotification({
                    type: 'info',
                    sticky: true,
                    title: lang.openImagesDownloadingIndex,
                    text: lang.openImagesDownloadingIndexText
                })
                sendData('stdout',`Downloading : ${trainAnnotationsBboxCsvUrl}`)
                downloadFileStream(trainAnnotationsBboxCsvUrl,trainAnnotationsBboxCsvFilePath,function(){
                    s.engineStatus.downloadingOpenImages = true
                    openImagesDownloadProcess = false
                    sendData('stdout','Source Files Downloaded. Restarting Request.')
                    runOpenImagesDownload(options,callback)
                })
                return
            }
            s.sendDashboardNotification({
                type: 'info',
                sticky: true,
                title: lang.openImagesDownloading,
                text: lang.openImagesDownloadingText
            })
            // loadCsv({
            //     path: trainAnnotationsBboxCsvFilePath,
            //     headers:
            // },function(trainAnnotationsBboxJson){
                var classDescriptionBoxableCsv = fs.readFileSync(classDescriptionBoxableCsvFilePath,'utf8').toString().split('\n')
                var classDescriptionBoxableJson = {}
                classDescriptionBoxableCsv.forEach(function(row){
                    var split = row.split(',')
                    if(split[1] && split[0])classDescriptionBoxableJson[split[1].trim()] = split[0].trim()
                })
                sendData('start')
                var checkClass = function(){
                    var className = options.classes[currentIteration]
                    var ind = currentIteration
                    if(!className){
                        //done
                        s.engineStatus.downloadingOpenImages = true
                        openImagesDownloadProcess = false
                        sendData('stop',`END`)
                        return
                    }
                    sendData('stdout',`Class ${ind} : ${className}`)
                    if(classDescriptionBoxableJson[className]){
                        var commandStr = "grep " + classDescriptionBoxableJson[className] + " " + trainAnnotationsBboxCsvFilePath
                        sendData('stdout',commandStr)
                        exec(commandStr,(err,response)=>{
                            var classAnnotations = response.toString().split('\n')
                            var totalNumOfAnnotations = classAnnotations.length
                            sendData('stdout',`Total number of annotations : ${totalNumOfAnnotations}`)
                            var cnt = 0
                            var numberOfAnnotations = classAnnotations.length
                            var identifyImage = function(filename,lineParts,lineData,fileDownloaded){
                                var fileName = lineParts[0]
                                var identify = execSync(`identify ${jpegImagesFolder}${filename}`,'utf8')
                                var dimensions = {}
                                if(!!identify){
                                    dimensions = identify.toString().split(' ')[2].split('x')
                                    dimensions = {
                                        width: parseFloat(dimensions[0]),
                                        height: parseFloat(dimensions[1].split('+')[0])
                                    }
                                    var labelFile = `${labelsFolder}${fileName}.txt`
                                    var annotationContents = [
                                        ind.toString(),
                                        ((parseFloat(lineParts[5]) + parseFloat(lineParts[4]))/2).toString(),
                                        ((parseFloat(lineParts[7]) + parseFloat(lineParts[6]))/2).toString(),
                                        (parseFloat(lineParts[5]) - parseFloat(lineParts[4])).toString(),
                                        (parseFloat(lineParts[7]) - parseFloat(lineParts[6])).toString()
                                    ].join(' ') + '\n'
                                    if(fs.existsSync(labelFile) && fs.readFileSync(labelFile,'utf8').indexOf(annotationContents) === -1){
                                        sendData('stdout','append : ' + annotationContents)
                                        fs.appendFileSync(labelFile,annotationContents)
                                    }else{
                                        sendData('stdout','write : ' + annotationContents)
                                        fs.writeFileSync(labelFile,annotationContents)
                                    }
                                    sendData('stdout','file : ' + fileName)
                                    if(fileDownloaded){
                                        var fileLink = '/trainer/jpegImages/list/' + filename
                                        sendDataToConnectedSuperUsersWithFilters({
                                            f:'jpegImageUploaded',
                                            source: 'Uploaded',
                                            name: filename,
                                            preview: fileLink,
                                            url: fileLink,
                                            annotation: fs.readFileSync(labelFile,'utf8'),
                                            className: className,
                                            dimensions: dimensions,
                                            hasAnnotation: true
                                        },[
                                            'preview',
                                            'url',
                                        ])
                                    }
                                }
                                return dimensions
                            }
                            var downloadImage = function(){
                                if(!openImagesDownloadAllowed){
                                    s.engineStatus.downloadingOpenImages = false
                                    openImagesDownloadProcess = false
                                    sendData('stop',`END`)
                                    return
                                }
                                var line = classAnnotations[cnt]
                                if(!line){
                                    ++currentIteration
                                    checkClass()
                                    return
                                }
                                sendData('stdout',`${className} : ${cnt} / ${numberOfAnnotations}`)
                                var lineParts = line.split(',')
                                if(!lineParts[2]){
                                    ++cnt
                                    downloadImage()
                                    return
                                }
                                var lineData = {}
                                trainAnnotationsBboxHeaders.forEach(function(key,place){
                                    lineData[key] = lineParts[place]
                                })
                                var labelFile = `${labelsFolder}${lineParts[0]}.txt`
                                var filename = `${lineParts[0]}.jpg`
                                if(!fs.existsSync(`${jpegImagesFolder}${filename}`)){
                                    downloadFile(`https://open-images-dataset.s3.amazonaws.com/${options.runMode}/${filename}`, `${jpegImagesFolder}${filename}`,function(){
                                        ++cnt
                                        downloadImage()
                                        identifyImage(filename,lineParts,lineData,true)
                                    })
                                }else{
                                    ++cnt
                                    identifyImage(filename,lineParts,lineData)
                                    downloadImage()
                                }
                            }
                            downloadImage()
                        })
                    }else{
                        sendData('stdout',`Class ${ind} : ${className} : ${lang['Not Found']}`)
                        ++currentIteration
                        checkClass()
                    }
                }
                var currentIteration = 0
                checkClass()
                response.ok = true
            // })
        }else{
            response.msg = lang['Process Already Running']
        }
        callback(response)
    }
    var stopOpenImagesDownload = function(callback){
        var response = {ok: false}
        if(openImagesDownloadProcess){
            s.appLog('ADD KILL FUNCTION')
            // openImagesDownloadProcess.kill('SIGTERM')
            response.ok = true
        }else{
            response.msg = lang['Process Not Running']
        }
        callback(response)
    }
    var testLastTrainedWeights = function(options,callback){
        //darknet.exe detector test cfg/coco.data yolov3.cfg yolov3.weights -ext_output dog.jpg
        var response = {ok: false}
        if(!options)options = {}
        var testCommand = [
            'detector',
            'test',
            darknetDataFilename,
            yoloCfgPath,
            weightsPath + '/yolov3-built_last.weights',
            '-ext_output',
            options.imagePath
        ]
        s.appLog(darknetCommand,testCommand.join(' '))
        weightTestingProcess = spawn(darknetCommand,testCommand)
        weightTestingProcess.stdout.on('data',function(d){
            sendDataToConnectedSuperUsers({
                f:'trainingProcess',
                ff:'stdout',
                data:d.toString()
            })
        })
        weightTestingProcess.stderr.on('data',function(d){
            var dataString = d.toString()
            sendDataToConnectedSuperUsers({
                f:'trainingProcess',
                ff:'stderr',
                data: dataString,
                lastFinishedIteration: 100
            })
        })
        weightTestingProcess.on('exit',function(d){
            callback(response)
        })
    }
    var weightTrainingProcess = null
    var beginTrainingProcess = function(options,callback){
        var response = {ok: false}
        if(!options)options = {}
        if(!options.gpus)options.gpus = '0'
        if(!options.darknetBase)options.darknetBase = `darknet53.conv.74`
        sendDataToConnectedSuperUsers({
            f:'trainingProcess',
            ff:'start',
        })
        s.sendDashboardNotification({
            type: 'success',
            title: lang.trainingStarting,
            text: lang.trainingStartingText
        })
        s.engineStatus.training = true
        if(weightTrainingProcess === null){
            fs.readFile(yoloCfgPath,'utf8',function(err,loadedCfg){
                var trainCommand = [
                    'detector',
                    'train',
                    darknetDataFilename,
                    yoloCfgPath,
                    baseWeightsPath + options.darknetBase,
                    // '-show_imgs',
                    '-gpus',
                    options.gpus,
                    '-dont_show'
                ]
                weightTrainingProcess = spawn(darknetCommand,trainCommand)
                s.appLog(darknetCommand,trainCommand.join(' '))
                sendDataToConnectedSuperUsers({
                    f:'trainingProcess',
                    ff:'stdout',
                    data:`${darknetCommand} ${trainCommand.join(' ')}`
                })
                weightTrainingProcess.stdout.on('data',function(d){
                    sendDataToConnectedSuperUsers({
                        f:'trainingProcess',
                        ff:'stdout',
                        data:d.toString()
                    })
                })
                var lastFinishedIteration = 100
                weightTrainingProcess.stderr.on('data',function(d){
                    var dataString = d.toString()
                    sendDataToConnectedSuperUsers({
                        f:'trainingProcess',
                        ff:'stderr',
                        data: dataString,
                        lastFinishedIteration: lastFinishedIteration
                    })
                })
                weightTrainingProcess.on('exit',function(d){
                    s.sendDashboardNotification({
                        type: 'warning',
                        hide: false,
                        title: lang.trainingStopped,
                        text: lang.trainingStoppedText
                    })
                    sendDataToConnectedSuperUsers({
                        f:'trainingProcess',
                        ff:'stop',
                    })
                    s.engineStatus.training = false
                    weightTrainingProcess = null
                    s.engineStatus.currentTrainingSession = {}
                })
                response.ok = true
            })
        }else{
            response.msg = lang['Process Already Running']
        }
        callback(response)
    }
    var stopTrainingProcess = function(callback){
        var response = {ok: false}
        if(weightTrainingProcess){
            weightTrainingProcess.kill('SIGINT')
            response.ok = true
        }else{
            response.msg = lang['Process Not Running']
            sendDataToConnectedSuperUsers({
                f:'trainingProcess',
                ff:'stop',
            })
            s.engineStatus.training = true
        }
        callback(response)
    }
    var lastCreateDarknetCfgOptions = {}
    var createDarknetCfg = function(options,callback,sendNotification){
        var gpuCount = 1
        if(!options)options = {}
        if(options.gpus){
            gpuCount = options.gpus.split(',').length || 1
        }else{
            options.gpus = '0'
        }
        switch(options.darknetBase){
            case'resnet50.65':
                if(!options.height)options.height = 256
                if(!options.width)options.width = 256
            break;
            case'yolov3-tiny.conv.15':
                if(!options.height)options.height = 224
                if(!options.width)options.width = 224
            break;
            case'yolov3-spp.conv.85':
                if(!options.height)options.height = 608
                if(!options.width)options.width = 608
            break;
            default: //darknet53.conv.74
                if(!options.height)options.height = 416
                if(!options.width)options.width = 416
            break;
        }
        var maxBatchesBase = options.maxBatchesBase || 2000
        if(options.classesCount < 2)maxBatchesBase = options.maxBatchesBase || 5200
        if(!options.batch)options.batch = 64
        if(!options.subdivisions)options.subdivisions = 64
        if(!options.width)options.width = config.darknetBaseWidth
        if(!options.height)options.height = config.darknetBaseHeight
        if(!options.learningRate)options.learningRate = 0.001 / gpuCount
        if(!options.burnIn)options.burnIn = 1000 * gpuCount // || 400
        if(!options.classesCount)options.classesCount = options.classes.length || 1
        if(!options.maxBatches)options.maxBatches = maxBatchesBase * options.classesCount  * gpuCount // || 5200
        if(!options.steps)options.steps = parseInt(options.maxBatches * 0.8)
        if(!options.filters){
            options.filters = (parseInt(options.classesCount) + 5) * 3
        }
        if(!options.outputFilePath)options.outputFilePath = config.activeFilesDir+ 'yolov3-built.cfg'
        var modifiedDarknetCfg = getDarknetCfgBase(options.darknetBase)
            .replace(/__BUILD_TAG/g,`Generated with Shinobi Neural Net Trainer`)
            .replace(/__BUILD_TIME/g,moment().format())
            .replace(/__GPU_COUNT/g,gpuCount)
            .replace(/__GPU_SELECTED/g,options.gpus)
            .replace(/__BATCH/g,options.batch)
            .replace(/__SUBDIVISIONS/g,options.subdivisions)
            .replace(/__WIDTH/g,options.width)
            .replace(/__HEIGHT/g,options.height)
            .replace(/__CLASSES_LIST/g,options.classes.join(', '))
            .replace(/__CLASSES/g,options.classesCount)
            .replace(/__FILTERS/g,options.filters)
            .replace(/__LEARNING_RATE/g,options.learningRate)
            .replace(/__BURN_IN/g,options.burnIn)
            .replace(/__MAX_BATCHES/g,options.maxBatches)
            .replace(/__STEPS/g,options.steps)
        fs.unlink(options.outputFilePath,function(){
            fs.writeFileSync(options.outputFilePath,modifiedDarknetCfg)
            lastCreateDarknetCfgOptions = options
            var response = {
                ok: true,
                path: options.outputFilePath,
                cfg: modifiedDarknetCfg,
                options: options
            }
            // if(sendNotification){
            //     s.sendDashboardNotification({
            //         type: 'info',
            //         title: lang.trainingCreatedDarknetData,
            //         text: lang.trainingCreatedDarknetDataText
            //     })
            // }
            if(callback)callback(response)
            return response
        })
    }
    var createDarknetData = function(options,callback){
        var gpuCount = 1
        if(!options)options = {}
        if(options.gpus)gpuCount = options.gpus.split(',').length || 1
        if(!options.classesCount)options.classesCount = options.classes.length || 1

        var classes = options.classes

        var joinedClassNames = classes.join('').replace(/ /g,'')
        // if(!fs.existsSync(classesNamesFile)){
            classes.forEach(function(dClass,key){
                var pushOperator = '>>'
                if(key === 0)pushOperator = '>'
                execSync(`echo "${dClass}" ${pushOperator} ${classesNamesFile}`)

            })
        // }
        execSync(`echo "classes = ${classes.length}" > ${darknetDataFilename}`)
        execSync(`echo "train  = ${trainTxtFilename}" >> ${darknetDataFilename}`)
        execSync(`echo "valid  = ${testTxtFilename}" >> ${darknetDataFilename}`)
        execSync(`echo "names = ${classesNamesFile}" >> ${darknetDataFilename}`)
        execSync(`echo "backup = ${weightsPath}" >> ${darknetDataFilename}`)
        try{
            fs.mkdirSync('weights')
        }catch(err){}
        var response = {
            ok: true,
        }
        if(callback)callback(response)
        return response
    }
    var downloadMarkedInternetImages = function(options,callback){
        if(!options)options = {}
        options.images.forEach(function(image){
            //download image (into jpegImagesFolder)
            //write label.txt (into labelsFolder)
            //add to training list (append to trainTxtFilename)
        })
        var response = {
            ok: true,
        }
        if(callback)callback(response)
        return response
    }
    //buildAnnotations
    var buildImageAnnotations = function(options,callback){
        var sendData = function(subAction,data){
            s.appLog('openImagesDownload',subAction,data)
            sendDataToConnectedSuperUsers({
                f: 'openImagesDownload',
                ff: subAction,
                data: data
            })
        }
        if(!options)options = {}
        if(!options.images)options.images = []
        var annotationBuildClassesHistory = getAnnotationClassesHistory()
        options.images.forEach(function(image){
            var filename = image.url.split('/')
            filename = filename[filename.length - 1]
            var filePath = jpegImagesFolder + filename
            var annotationFilename = getLabelFileName(filename)
            var annotationFilePath = labelsFolder + annotationFilename
            var foundClassNames = []
            var annotationContents = image.dataAnnotation || image.annote || ''
            if(annotationContents && annotationContents.trim().length > 0 && image.boundingBoxes.length > 0){
                image.boundingBoxes.forEach(function(box){
                    if(foundClassNames.indexOf(box.tag) === -1)foundClassNames.push(box.tag)
                    if(!annotationBuildClassesHistory[box.tag])annotationBuildClassesHistory[box.tag] = {}
                    annotationBuildClassesHistory[box.tag][filename] = 1
                })
                // check for stale remnant of this image in other classes
                Object.keys(annotationBuildClassesHistory).forEach(function(tag){
                    if(foundClassNames.indexOf(tag) === -1 && annotationBuildClassesHistory[tag][filename]){
                        delete(annotationBuildClassesHistory[tag][filename])
                    }
                })
                //
                // if(fs.existsSync(annotationFilePath) && fs.readFileSync(annotationFilePath,'utf8').indexOf(annotationContents) === -1){
                //     sendData('stdout','append : ' + annotationContents)
                //     fs.appendFileSync(annotationFilePath,annotationContents)
                // }else{
                //     sendData('stdout','write : ' + annotationContents)
                //     fs.writeFileSync(annotationFilePath,annotationContents)
                // }
                fs.unlink(annotationFilePath,function(){
                    sendData('stdout',`write : ${filename} : ` + annotationContents)
                    fs.writeFile(annotationFilePath,annotationContents,function(err){
                        if(err){
                            s.appLog(err)
                        }
                    })
                })
                switch(image.source){
                    case'Uploaded':
                        // local link

                    break;
                    default:
                        // remote link
                        fs.stat(filePath,function(err){
                            if(err){
                                downloadFile(image.url,filePath)
                            }
                        })
                    break;
                }
            }
        })
        fs.writeFile(annotationBuildClassesHistoryPath,s.prettyPrint(annotationBuildClassesHistory),function(err){
            if(err)s.appLog(err)
        })
        var response = {
            ok: true,
        }
        if(callback)callback(response)
        s.sendDashboardNotification({
            type: 'info',
            title: lang.builtImageAnnotations,
            text: lang.builtImageAnnotationsText
        })
        return response
    }
    var splitTrainAndTestFiles = function(options,callback){
        if(!options)options = {}
        if(weightTrainingProcess === null){
            s.sendDashboardNotification({
                type: 'warning',
                hide: false,
                title: lang.trainingSplitTrainAndTest,
                text: lang.trainingSplitTrainAndTestText
            })
            getLocalImagesBasedOnClass({
                classes: options.classes
            },function(files){
                s.appLog(files.length)
                s.appLog(options.classes.length)
                s.appLog(s.s(options.classes))
                s.engineStatus.currentTrainingSession = {
                    numberOfImages: files.length,
                    numberOfClasses: options.classes.length,
                    classes: options.classes,
                }
                if(fs.existsSync(trainTxtFilename))fs.unlinkSync(trainTxtFilename)
                if(fs.existsSync(testTxtFilename))fs.unlinkSync(testTxtFilename)
                var testLength = parseInt(files.length * 0.1)
                var testArray = []
                for (i = 0; i < testLength; i++) {
                    var randomKey = Math.floor(Math.random() * files.length)
                    testArray.push(randomKey)
                }
                var trainTxtContents = []
                var testTxtContents = []
                files.forEach(function(filename,key){
                    if(filename.indexOf('.jpg') > -1){
                        var fileRow = jpegImagesFolder + filename
                        if(testArray.indexOf(key) > -1){
                            testTxtContents.push(fileRow)
                        }else{
                            trainTxtContents.push(fileRow)
                        }
                    }
                })
                fs.writeFileSync(trainTxtFilename,trainTxtContents.join('\n'),'utf8')
                fs.writeFileSync(testTxtFilename,testTxtContents.join('\n'),'utf8')
                if(callback)callback(response)
            })
        }else{
            if(callback)callback(response)
        }
        var response = {
            ok: true,
        }
        return response
    }
    var parseNvidiaSmi = function(callback){
        var response = {
            ok: true,
        }
        var newArray = []
        const addGpu = (gpu) => {
            newArray.push({
                id: gpu.minor_number,
                name: gpu.product_name,
                brand: gpu.product_brand,
                fan_speed: gpu.fan_speed,
                temperature: gpu.temperature,
                power: gpu.power_readings,
                utilization: gpu.utilization,
                maxClocks: gpu.max_clocks,
            })
        }
        exec('nvidia-smi -x -q',function(err,data){
            var response = xmlParser.toJson(data)
            try{
                const parsedSmi = JSON.parse(response)
                if(parsedSmi instanceof Array){
                    parsedSmi.nvidia_smi_log.gpu.forEach((gpu)=>{
                        addGpu(gpu)
                    })
                }else{
                    addGpu(parsedSmi.nvidia_smi_log.gpu)
                }
            }catch(err){
                console.log(err,response)
            }
            if(callback)callback(newArray)
        })
    }
    parseNvidiaSmi((response)=>{
        s.gpuDataOnStart = response
    })
    app.get('/getGpuData', function (req,res){
        parseNvidiaSmi(function(response){
            res.setHeader('Content-Type', 'application/json')
            res.end(s.prettyPrint(response))
        })
    })
    app.get('/:apiKey/trainer/searchInternetImages', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            // {
            //     by: resp.$user.mail,
            //     ip: resp.ip,
            // }
            res.setHeader('Content-Type', 'application/json')
            var searchTerm = req.query.search
            var itemCount = parseInt(req.query.limit) || 20
            var searchResults = []
            searchPixaBay({
                term: searchTerm,
                limit: itemCount
            },function(err,data){
                if(data)searchResults = searchResults.concat(data)
                sendDataToConnectedSuperUsers({
                    f:'searchPixaBay',
                    response: data
                })
                res.end(s.prettyPrint(data))
            })
        },res,req)
    })
    app.all('/:apiKey/trainer/openImagesDownload/start', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var form = s.getPostData(req)
            form.auth = req.params.apiKey
            runOpenImagesDownload(form,function(err,data){
                res.end(s.prettyPrint(data))
            })
        },res,req)
    })
    app.all('/:apiKey/trainer/openImagesDownload/stop', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            openImagesDownloadAllowed = false
            res.end(s.prettyPrint({ok: true}))
        },res,req)
    })
    app.get('/:apiKey/trainer/downloadMarkedInternetImages', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            // {
            //     by: resp.$user.mail,
            //     ip: resp.ip,
            // }
            res.setHeader('Content-Type', 'application/json')
            var form = s.getPostData(req)
            downloadMarkedInternetImages({
                images: form.images,
            },function(err,data){
                sendDataToConnectedSuperUsers({
                    f:'downloadMarkedInternetImages',
                    response: data
                })
                res.end(s.prettyPrint(data))
            })
        },res,req)
    })
    app.all('/:apiKey/trainer/train', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var form = s.getPostData(req)
            sendDataToConnectedSuperUsers({
                f:'trainingProcess',
                ff:'starting',
            })
            createDarknetData(form,function(response){
                createDarknetCfg(form,function(response){
                    splitTrainAndTestFiles({
                        classes: form.classes
                    },function(response){
                        beginTrainingProcess(form,function(response){
                            sendDataToConnectedSuperUsers({
                                f:'trainingProcess',
                                response: response
                            })
                            res.end(s.prettyPrint(response))
                        })
                    })
                },true)
            })
        },res,req)
    })
    app.get('/:apiKey/trainer/test', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'image/jpeg')
            const downloadedFile = config.activeFilesDir + `${new Date().getTime()}.jpg`
            downloadFile(req.query.imageUrl,downloadedFile,()=>{
                testLastTrainedWeights({
                    imagePath: downloadedFile
                },function(response){
                    fs.unlink(downloadedFile,()=>{
                        exec('rm ' + downloadedFile,()=>{

                        })
                    })
                    const predictionsImageLocation = `./predictions.jpg`
                    fs.createReadStream(predictionsImageLocation).pipe(res)
                    res.on('close',()=>{
                        fs.unlink(predictionsImageLocation,()=>{
                            exec('rm ' + predictionsImageLocation,()=>{

                            })
                        })
                    })
                })
            })
        },res,req)
    })
    app.all('/:apiKey/trainer/train/stop', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            stopTrainingProcess(function(response){
                sendDataToConnectedSuperUsers({
                    f:'stopTrainingProcess',
                    response: response
                })
                res.end(s.prettyPrint(response))
            })
        },res,req)
    })
    app.all('/:apiKey/trainer/darknetCfg/create', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var form = s.getPostData(req)
            var formOptions = {
                gpus: form.gpus,
                batch: form.batch,
                subdivision: form.subdivision,
                width: form.width,
                height: form.height,
                darknetBase: form.darknetBase,
                classes: form.classes,
                maxBatchesBase: form.maxBatchesBase,
                outputFilePath: form.outputFilePath,
            }
            createDarknetData(form,function(response){
                createDarknetCfg(formOptions,function(response){
                    sendDataToConnectedSuperUsers({
                        f:'createDarknetCfg',
                        response: response
                    })
                    res.end(s.prettyPrint(response))
                },true)
            })
        },res,req)
    })
    app.all([
        '/:apiKey/trainer/darknetCfg/get',
        '/:apiKey/trainer/darknetCfg/get/:type'
    ], function (req,res){
        res.setHeader('Content-Type', 'application/json')
        s.authenticateWithUsernamePassword(req.params,function(resp){
            var darknetBase = req.query.base
            if(req.params.type === 'base'){
                res.end(s.prettyPrint({ok: true, cfg: getDarknetCfgBase(darknetBase), options: lastCreateDarknetCfgOptions}))
            }else{
                var builtCfg = config.activeFilesDir+ 'yolov3-built.cfg'
                fs.stat(builtCfg,function(err){
                    if(!err){
                        fs.readFile(builtCfg,function(err,data){
                            res.end(s.prettyPrint({ok: true, cfg: data.toString(), options: lastCreateDarknetCfgOptions}))
                        })
                    }else{
                        res.end(s.prettyPrint({ok: true, cfg: getDarknetCfgBase(darknetBase), options: lastCreateDarknetCfgOptions}))
                    }
                })
            }
        },res,req)
    })
    app.use(fileUpload())
    app.all('/:apiKey/trainer/jpegImages/list', function(req, res) {
        res.setHeader('Content-Type', 'application/json')
        s.authenticateWithUsernamePassword(req.params,function(resp){
            var form = s.getPostData(req)
            var rawList = Object.keys(getManuallyUploadedFileList())
            var rawJpegList = []
            rawList.forEach(function(name){
                if(name.indexOf('.jpg') > -1 || name.indexOf('.jpeg') > -1){
                    rawJpegList.push(name)
                }
            })
            var parsedList = []
            rawJpegList.forEach(function(filename,key){
                var annotationFilename = fs.existsSync(labelsFolder + getLabelFileName(filename))
                var fileLink = '/trainer/jpegImages/list/' + filename
                parsedList.push({
                    source: 'Uploaded',
                    name: filename,
                    preview: fileLink,
                    url: fileLink,
                    hasAnnotation: annotationFilename
                })
            })
            res.end(s.prettyPrint(parsedList))
        },res,req)
    })
    app.get([
        '/trainer/jpegImages/list/:file',
        '/:apiKey/trainer/jpegImages/list/:file',
    ], function(req, res) {
        // s.authenticateWithUsernamePassword(req.params,function(resp){
            var filePath = jpegImagesFolder + req.params.file
            fs.stat(filePath,function(err){
                if(!err){
                    res.setHeader('Content-Type', 'image/jpeg')
                    fs.createReadStream(filePath).pipe(res)
                }else{
                    res.end('No file found')
                }
            })
        // },res,req)
    })
    app.get('/:apiKey/trainer/jpegImages/list/:file/delete', function(req, res) {
        res.setHeader('Content-Type', 'application/json')
        var response = {
            ok: false
        }
        s.authenticateWithUsernamePassword(req.params,function(resp){
            var filePath = jpegImagesFolder + req.params.file
            fs.stat(filePath,function(err){
                if(!err){
                    var manuallyUploadedFiles = getManuallyUploadedFileList()
                    delete(manuallyUploadedFiles[req.params.file])
                    fs.unlink(filePath,function(err){
                        if(err)s.appLog(err)
                        sendDataToConnectedSuperUsers({
                            f:'jpegImageDeleted',
                            source: 'Uploaded',
                            name: req.params.file,
                        })
                        response.ok = true
                        res.send(s.prettyPrint(response))
                        writeManuallyUploadedFileList(manuallyUploadedFiles)
                    })
                }else{
                    res.send(s.prettyPrint(response))
                }
            })
        },res,req)
    })
    app.get('/:apiKey/trainer/jpegImages/list/:file/test', function(req, res) {
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'image/jpeg')
            const filePath = jpegImagesFolder + req.params.file
            testLastTrainedWeights({
                imagePath: filePath
            },function(response){
                const predictionsImageLocation = `./predictions.jpg`
                fs.createReadStream(predictionsImageLocation).pipe(res)
                res.on('close',()=>{
                    fs.unlink(predictionsImageLocation,()=>{
                        exec('rm ' + predictionsImageLocation,()=>{

                        })
                    })
                })
            })
        },res,req)
    })
    app.post('/:apiKey/trainer/jpegImages/upload', function(req, res) {
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var fileKeys = Object.keys(req.files)
            if (fileKeys.length == 0) {
                return res.status(400).send('No files were uploaded.');
            }
            var manuallyUploadedFiles = getManuallyUploadedFileList()
            var matrices = s.parseJSON(req.body.matrices)
            var labelData = null
            var dimensions = null
            if(req.body.labelFilename && matrices){
                labelData = []
                dimensions = {
                    height: req.body.height,
                    width: req.body.width,
                }
                matrices.forEach(function(matrix){
                    labelData.push(matrix.annote)
                })
                if(labelData.length > 0){
                    fs.writeFile(labelsFolder + req.body.labelFilename,labelData.join('\n'),function(err){
                        if(err)s.appLog(err)
                    })
                }
            }
            fileKeys.forEach(function(key){
                var file = req.files[key]
                if(file.name.indexOf('.jpg') > -1){
                    manuallyUploadedFiles[file.name] = {}
                    file.mv(jpegImagesFolder + file.name, function(err) {
                        if (err)return s.systemLog(err)
                        var fileLink = '/' + req.params.apiKey + '/trainer/jpegImages/list/' + file.name
                        var webSocketResponse = {
                            f:'jpegImageUploaded',
                            source: 'Uploaded',
                            name: file.name,
                            preview: fileLink,
                            url: fileLink,
                        }
                        if(matrices){
                            webSocketResponse = Object.assign(webSocketResponse,{
                                boundingBoxes: matrices,
                                dimensions: dimensions,
                            })
                        }
                        sendDataToConnectedSuperUsers(webSocketResponse)
                    })
                }
            })
            writeManuallyUploadedFileList(manuallyUploadedFiles)
            var response = {
                ok: true
            }
            res.send(s.prettyPrint(response))
        },res,req)
    })
    app.all('/:apiKey/trainer/imageAnnotations/build', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var form = s.getPostData(req)
            var formOptions = {
                classes: form.classes,
                images: form.images,
            }
            buildImageAnnotations(formOptions,function(response){
                sendDataToConnectedSuperUsers({
                    f:'buildImageAnnotations',
                    response: response
                })
                res.end(s.prettyPrint(response))
            })
        },res,req)
    })
    app.get('/:apiKey/models/create', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            modelManager.create((response)=>{
                s.sendDashboardNotification({
                    type: 'success',
                    hide: false,
                    title: lang.builtModels,
                    text: lang.builtModelsText
                })
                res.end(s.prettyPrint(response))
            })
        },res,req)
    })
    app.get('/:apiKey/models/clean', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            modelManager.clean((response)=>{
                res.end(s.prettyPrint(response))
            })
        },res,req)
    })
    io.on('connect',function(cn){
        cn.on('super',function(d){
            switch(d.f){
                case'buildImageAnnotations':
                    var formOptions = {
                        classes: d.form.classes,
                        images: d.form.images,
                    }
                    buildImageAnnotations(formOptions,function(response){
                        sendDataToConnectedSuperUsers({
                            f:'buildImageAnnotations',
                            response: response
                        })
                    })
                break;
            }
        })
    })
}
