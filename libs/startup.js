var fs = require('fs')
var exec = require('child_process').exec
var spawn = require('child_process').spawn
var commandExists = require('command-exists');
var request = require('request');
var AdmZip = require('adm-zip');
module.exports = function(s,config,lang,app,io){
    config.userHasSubscribed = false
    var checkSubscription = function(callback){
        if(config.subscriptionId && config.subscriptionId !== 'sub_XXXXXXXXXXXX'){
            var url = 'https://licenses.shinobi.video/subscribe/check?subscriptionId=' + config.subscriptionId + '&productType=Byaku'
            request(url,{
                method: 'GET',
                timeout: 30000
            }, function(err,resp,body){
                var json = s.parseJSON(body)
                if(err)console.log(err,json)
                var hasSubcribed = json && !!json.ok
                config.userHasSubscribed = hasSubcribed
                var registeredAmount = 0
                if(hasSubcribed && json.subscriptionPlans){
                    var foundGrandfatherPlan = false
                    s.subscriptionPlansRegistered = json.subscriptionPlans
                    json.subscriptionPlans.forEach((subscription)=>{
                        //if earlier than Tue Apr 14 2020 11:29:26 GMT-0700 (Pacific Daylight Time)
                        if(new Date(subscription.created * 1000) < 1586888966402){
                            foundGrandfatherPlan = true
                        }
                        //add up amounts (make equal or higher than 10 to be valid)
                        registeredAmount += subscription.plan.amount / 100
                    })
                    if(foundGrandfatherPlan || registeredAmount >= 10){
                        hasSubcribed = true
                    }else{
                        hasSubcribed = false
                    }
                }else{
                    s.subscriptionPlansRegistered = []
                }
                callback(hasSubcribed)
            })
        }else{
            callback(false)
        }
    }
    const checkForDarknet = (callback) => {
        var noDarknetBuildError = true
        fs.stat('./darknet',(err,stats) => {
            if(err){
                var darknetZipLocation = './darknet.zip'
                s.appLog('Downloading Darknet... Please Wait...')
                request(`https://github.com/AlexeyAB/darknet/archive/master.zip`)
                  .pipe(fs.createWriteStream(darknetZipLocation))
                  .on('close', function () {
                      s.appLog('Download Complete. darknet.zip Created.')
                      console.log('File written!');
                      var zip = new AdmZip(darknetZipLocation);
                      s.appLog('Extracting darknet.zip...')
                      zip.extractAllTo("./darknet/", true);
                      s.appLog('Extraction Complete.')
                      fs.unlink(darknetZipLocation,()=>{})
                      const darknetBuildScriptLocation = './buildDarknet.sh'
                      const darknetBuildScript = `#!/bin/bash
                      cd ./darknet/darknet-master
                      export PATH=/usr/local/cuda/bin:$PATH
                      export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
                      sed -i 's/GPU=0/GPU=1/g' Makefile
                      sed -i 's/CUDNN=0/CUDNN=1/g' Makefile
                      if [ -x "$(command -v opencv_version)" ]; then
                          sed -i 's/OPENCV=0/OPENCV=1/g' Makefile
                      fi
                      make`
                      fs.writeFile(darknetBuildScriptLocation,darknetBuildScript,()=>{
                          const buildingDarknet = spawn('sh',[darknetBuildScriptLocation])
                          buildingDarknet.stdout.on('data',(data)=>{
                              s.appLog('darknetBuildOut',data.toString())
                          })
                          buildingDarknet.stderr.on('data',(data)=>{
                              var string = data.toString()
                              if(string.indexOf('unsupported GNU version! gcc versions later than') > -1){
                                  noDarknetBuildError = false
                                  s.appLog('Unsupported GCC version!')
                                  s.appLog('FAILED TO BUILD DARKNET','DELETE DARKNET FOLDER AND RESTART BYAKU')
                                  s.appLog('Run the following in terminal to fix GCC/G++ version.','bash <(curl -s https://gitlab.com/Shinobi-Systems/supplements/-/raw/master/downgradeGccG++.sh)')
                              }
                              s.appLog('darknetBuildErr',string)
                          })
                          buildingDarknet.on('exit',(data)=>{
                              if(noDarknetBuildError){
                                  s.appLog('Done Building Darknet, Happy Training!')
                              }else{
                                  s.appLog('There has been an error building Darknet.')
                              }
                              fs.unlink(darknetBuildScriptLocation,()=>{
                                  exec('rm ' + darknetBuildScriptLocation,()=>{})
                              })
                          })
                      })
                   });
            }
            if(callback)callback()
        })
    }
    const checkForAccountRegistryFile = (callback) => {
        //create login accounts file
        fs.stat(__dirname + '/super.json',(err,stat)=>{
            if(err){
                var superSample = require('./../super.sample.json')
                fs.writeFileSync('./super.json',s.prettyPrint(superSample))
                console.log('Default Login is admin : admin')
            }else{
                console.log('Login details found in super.json')
            }
            if(callback)callback()
        })
    }
    const checkForImageMagick = (callback) => {
        commandExists('identify', function(err, commandExists) {
            if(!commandExists) {
                s.appLog('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                s.appLog('ImageMagick is not installed!!!')
            }
            if(callback)callback()
        })
    }
    checkForAccountRegistryFile(()=>{
        checkForImageMagick(()=>{
            checkForDarknet(()=>{
                checkSubscription((subscribed)=>{
                    s.appLog(`Byaku is${subscribed ? '' : ' NOT'} Activated.${subscribed ? '' : ' Check out https://licenses.shinobi.video'}`)
                    s.appLog('Byaku is created by Shinobi Systems, Moinul Alam')
                    s.appLog('https://shinobi.systems')
                })
            })
        })
    })
}
