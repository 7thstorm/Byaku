var fs = require('fs');
module.exports = function(s,config,lang){
    //Authenticator functions
    s.api = {}
    s.superUsersApi = {}
    s.factorAuth = {}
    s.failedLoginAttempts = {}
    s.activeSessions = {}
    s.authenticateWithUsernamePassword = function(params,finish,res,req){
        if(req){
            //express (http server) use of auth function
            var failed=function(){
                if(!req.ret){req.ret={ok:false}}
                req.ret.msg=lang['Not Authorized'];
                res.end(s.s(req.ret));
            }
        }else{
            //socket.io use of auth function
            var failed = function(){
                //maybe log
                finish({ok: false})
            }
        }
        var clearAfterTime=function(){
            //remove temp key from memory
            clearTimeout(s.activeSessions[params.apiKey].timeout)
            s.activeSessions[params.apiKey].timeout = setTimeout(function(){
                delete(s.activeSessions[params.apiKey])
            },1000*60*5)
        }
        //check if key is already in memory to save query time
        if(!params.ignoreActiveSessions && s.activeSessions[params.apiKey] && params.forceQuery !== true){
            finish(s.activeSessions[params.apiKey])
            if(s.activeSessions[params.apiKey].timeout){
               clearAfterTime()
            }
        }else{
            if(s.activeSessions[params.apiKey]){
                clearTimeout(s.activeSessions[params.apiKey].timeout)
            }
            //not using plain login
            s.superAuth({
                mail: params.name,
                pass: params.pass
            },function(response){
                if(response.ok !== false){
                    s.activeSessions[params.apiKey] = {
                        foundData: response.$user,
                        user: response.$user
                    }
                    finish(s.activeSessions[params.apiKey])
                }else{
                    failed()
                }
            },res,req)
        }
    }
    //super user authentication handler
    s.superAuth = function(params,callback,res,req){
        var userFound = false
        var userSelected = false
        var adminUsersSelected = null
        try{
            var success = function(){
                var chosenConfig = config
                if(req && res){
                    chosenConfig = s.getConfigWithBranding(req.hostname)
                    res.setHeader('Content-Type', 'application/json')
                    var ip = req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress;
                    var resp = {
                        ok: userFound,
                        ip: ip
                    }
                    if(userFound === false){
                        resp.msg = lang['Not Authorized']
                        res.end(s.prettyPrint(resp))
                    }
                    if(userSelected){
                        resp.$user = userSelected
                    }
                    if(adminUsersSelected){
                        resp.users = adminUsersSelected
                    }
                }
                callback({
                    ip : ip,
                    $user: userSelected,
                    users: adminUsersSelected,
                    config: chosenConfig,
                    lang: lang
                })
            }
            if(params.auth && s.superUsersApi[params.auth]){
                userFound = true
                userSelected = s.superUsersApi[params.auth].$user
                success()
            }else{
                var superUserList = JSON.parse(fs.readFileSync('./super.json'))
                superUserList.forEach(function(superUser,n){
                    if(
                        userFound === false &&
                        (
                            params.auth && superUser.tokens && superUser.tokens[params.auth] || //using API key (object)
                            params.auth && superUser.tokens && superUser.tokens.indexOf && superUser.tokens.indexOf(params.auth) > -1 || //using API key (array)
                            (
                                params.mail && params.mail.toLowerCase() === superUser.mail.toLowerCase() && //email matches
                                (
                                    params.pass === superUser.pass || //user give it already hashed
                                    superUser.pass === s.createHash(params.pass) || //hash and check it
                                    superUser.pass.toLowerCase() === s.md5(params.pass).toLowerCase() //check if still using md5
                                )
                            )
                        )
                    ){
                        userFound = true
                        userSelected = superUser
                        success()
                    }
                })
            }
        }catch(err){
            s.appLog('The following error may mean your super.json is not formatted correctly.')
            s.appLog(err)
        }
        if(userFound === true){
            return true
        }else{
            if(res)res.end(s.prettyPrint({
                ok: false,
                msg: lang['Not Authorized']
            }))
            return false
        }
    }
}
